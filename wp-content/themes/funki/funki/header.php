<!doctype html>
<html <?php echo themify_get_html_schema(); ?> <?php language_attributes(); ?>>
<head>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-9091567331130676",
        enable_page_level_ads: true
    });
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146000551-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-146000551-1');
</script>


<?php
/** Themify Default Variables
 @var object */
	global $themify; ?>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<title itemprop="name"><?php wp_title( '' ); ?></title>

<?php
/**
 *  Stylesheets and Javascript files are enqueued in theme-functions.php
 */
?>

<!-- wp_header -->
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php themify_body_start(); //hook ?>
<div id="bgwrap">
	<div id="pagewrap" class="hfeed site">
    
    <div id="headerwrap">
    
    		<?php themify_header_before(); //hook ?>
            <header id="header">
            	<?php themify_header_start(); //hook ?>
                <hgroup>
                    <?php echo themify_logo_image('site_logo'); ?>
    
                    <?php if ( $site_desc = get_bloginfo('description') ) : ?><h2 id="site-description"><?php echo $site_desc; ?></h2><?php endif; ?>
                </hgroup>
    
                <div class="header-widget">
                    <?php dynamic_sidebar('header-widget'); ?>
                </div>
                <!--/header-widget -->
        
                <div class="social-widget">
                    <?php dynamic_sidebar('social-widget'); ?>
                    <?php if(!themify_check('setting-exclude_rss')): ?>
                        <div class="rss"><a href="<?php if(themify_get('setting-custom_feed_url') != ""){ echo themify_get('setting-custom_feed_url'); } else { bloginfo('rss2_url'); } ?>">RSS</a></div>
                    <?php endif ?>
                </div>
                <!--/social widget --> 
    
                <nav id="main-nav-wrap">
                    <div id="menu-icon" class="mobile-button"></div>
    
                    <?php if (function_exists('wp_nav_menu')) {
                        wp_nav_menu(array('theme_location' => 'main-nav' , 'fallback_cb' => 'themify_default_main_nav' , 'container'  => '' , 'menu_id' => 'main-nav' , 'menu_class' => 'main-nav'));
                    } else {
                        themify_default_main_nav();
                    } ?>
                </nav>
		<!-- /#main-nav -->
                
		<?php if(!themify_check('setting-exclude_search_form')): ?>
			<div id="searchform-wrap">
				<div id="search-icon" class="mobile-button"></div>
				<?php get_search_form(); ?>
			</div>
			<!-- /searchform-wrap -->
                <?php endif ?>
				
                <?php themify_header_end(); //hook ?>
            </header>
            <!--/header -->
            <?php themify_header_after(); //hook ?>
            
        </div>
        <!-- /headerwrap -->
		
		<div id="body" class="clearfix">
        <?php themify_layout_before(); //hook ?>