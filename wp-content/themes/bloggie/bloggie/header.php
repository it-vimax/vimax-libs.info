<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo themify_get_html_schema(); ?> <?php language_attributes(); ?>>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146000551-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-146000551-1');
</script>


<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-9091567331130676",
        enable_page_level_ads: true
    });
</script>

<?php

/** Themify Default Variables
 @var object */
	global $themify; ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title itemprop="name"><?php wp_title( '' ); ?></title>

<?php
/**
 *  Stylesheets and Javascript files are enqueued in theme-functions.php
 */
?>

<!-- wp_header -->
<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>

<?php themify_body_start(); //hook ?>
<div id="pagewrap" class="hfeed site">

	<div id="topbar">
		<div class="pagewidth">
			<div class="topbar-nav">
	
			<?php
			if (function_exists('wp_nav_menu')) {
				wp_nav_menu(array('theme_location' => 'secondary-nav' , 'fallback_cb' => 'default_secondary_nav' , 'container'  => '' , 'menu_id' => 'secondary-nav'));
			}
			else {
				themify_default_main_nav();
			}
			?>
		
			</div><!--/topbar-nav -->
		
			<div class="social-widget">
				<?php dynamic_sidebar('social-widget'); ?>

				<?php if(!themify_check('setting-exclude_rss')): ?>
					<div class="rss"><a href="<?php if(themify_get('setting-custom_feed_url') != ""){ echo themify_get('setting-custom_feed_url'); } else { echo bloginfo('rss2_url'); } ?>">RSS</a></div>
				<?php endif ?>

			</div><!--/topbar-widget -->
		
		</div>
	</div>
	<!--/topbar -->
	<div class="pagewidth">
    	<div id="headerwrap">
        <?php themify_header_before(); //hook ?>
		<div id="header">
        	<?php themify_header_start(); //hook ?>
			
			<?php echo themify_logo_image('site_logo'); ?>

	
			<?php if ( $site_desc = get_bloginfo('description') ) : ?><div id="site-description"><?php echo $site_desc; ?></div><?php endif; ?><!-- /#site-description -->
	
			<?php
			if (function_exists('wp_nav_menu')) {
				wp_nav_menu(array('theme_location' => 'main-nav' , 'fallback_cb' => 'themify_default_main_nav' , 'container'  => '' , 'menu_id' => 'main-nav' , 'menu_class' => 'main-nav'));
			}
			else {
				themify_default_main_nav();
			}
			?>
			
			<?php if(!themify_check('setting-exclude_search_form')): ?>
				<?php get_search_form(); ?>
			<?php endif ?>
	
			<div class="header-ad">
				<?php dynamic_sidebar('header-widget'); ?>
			</div>
			<!-- /header-ad --> 
			
			<div id="nav-bar"></div><!-- /nav bar -->
            
			<?php themify_header_end(); //hook ?>
		</div><!-- /header -->
        <?php themify_header_after(); //hook ?>
        </div><!-- /headerwrap -->
		
		<div id="body">
        <?php themify_layout_before(); //hook ?>